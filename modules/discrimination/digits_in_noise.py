from .set_window import DigitsInNoiseWindow
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSlot, Qt
import time


class DigitsInNoiseModule(DigitsInNoiseWindow):
    def __init__(self, parent):
        super(DigitsInNoiseModule, self).__init__(parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        ql_response = self.dialog.findChildren(QtWidgets.QLineEdit, "q_line_edit_answer")[0]
        ql_response.returnPressed.connect(self.on_response)
        ql_response.setEnabled(False)
        pb_start = self.dialog.findChildren(QtWidgets.QPushButton, "q_button_start")[0]
        pb_start.custom_clicked.connect(self.on_start_run)
        self._finish_message = ''

    @pyqtSlot()
    def on_response(self):
        _edit_line = self.sender()
        response = _edit_line.text()
        _edit_line.setText('')
        self.signal_response.emit(response)

    @pyqtSlot()
    def on_start_run(self):
        _button = self.sender()
        _button.setEnabled(False)
        QtWidgets.QApplication.processEvents()
        self.signal_start.emit()
        _button.setText('')

    def on_feedback(self, value: bool):
        pb_start = self.dialog.findChildren(QtWidgets.QPushButton, "q_button_start")[0]
        if value:
            pb_start.setStyleSheet("font-size:32px; background-color: green")
        else:
            pb_start.setStyleSheet("font-size:32px; background-color: red")
        QtWidgets.QApplication.processEvents()
        time.sleep(0.5)
        pb_start.setStyleSheet("font-size:32px")
        QtWidgets.QApplication.processEvents()

    def on_finish(self):
        ql_response = self.dialog.findChildren(QtWidgets.QLineEdit, "q_line_edit_answer")[0]
        ql_response.setEnabled(False)
        pb_start = self.dialog.findChildren(QtWidgets.QPushButton, "q_button_start")[0]
        pb_start.setText(self._finish_message)
        QtWidgets.QApplication.processEvents()
        time.sleep(3)
        pb_start.setEnabled(True)
        # self.dialog.hide()

    def initialize(self, ini_message: str = 'Start', end_message: str = ''):
        self._finish_message = end_message
        QtWidgets.QApplication.processEvents()
        pb_start = self.dialog.findChildren(QtWidgets.QPushButton, "q_button_start")[0]
        pb_start.setText(ini_message)
        self.dialog.showFullScreen()
        QtWidgets.QApplication.processEvents()

    @pyqtSlot()
    def start_playing(self):
        ql_response = self.dialog.findChildren(QtWidgets.QLineEdit, "q_line_edit_answer")[0]
        ql_response.setEnabled(False)

    @pyqtSlot()
    def stop_playing(self):
        ql_response = self.dialog.findChildren(QtWidgets.QLineEdit, "q_line_edit_answer")[0]
        ql_response.setEnabled(True)
        ql_response.setFocus()

    def set_finish_message(self, value):
        self._finish_message = value

    def get_finish_message(self):
        return self._finish_message

    finish_message = property(get_finish_message, set_finish_message)











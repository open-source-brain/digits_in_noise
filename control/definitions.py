from pysignal_generator.noise_functions import generate_modulated_noise, generate_shaped_noise
from pysignal_generator.tools import delay_signal
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QWidget
import random
import os.path
import pandas as pd
import soundfile as sf
import sounddevice as sd
from os.path import isfile, join
from os import listdir
import numpy as np
import os
import time
from modules.discrimination import digits_in_noise as dn
from pathlib import Path
import datetime
import re


class AdaptiveTrackingParameters(object):
    def __init__(self):
        self.n_reversals_for_mean = 6
        self.start_values = [6]
        self.step_sizes = [-10, -5, -2]
        self.back_step_sizes = [8, 3, 2]
        self.n_reversals_per_step = [3, 2, 4]
        self.n_correct_down = 2
        self.n_incorrect_up = 1
        self.tracked_parameter = ''


class AdaptiveTracker(object):
    def __init__(self, parameters: AdaptiveTrackingParameters = AdaptiveTrackingParameters()):
        self.results = pd.DataFrame()
        self.summary = pd.DataFrame()
        self.extra_parameters = {}

        # set parameters
        self._step_sizes = parameters.step_sizes
        self._back_step_sizes = parameters.back_step_sizes
        self._n_reversals_per_step = parameters.n_reversals_per_step
        self._start_values = parameters.start_values
        self._n_reversals_for_mean = parameters.n_reversals_for_mean
        self._n_correct_down = parameters.n_correct_down
        self._n_incorrect_up = parameters.n_incorrect_up
        self._tracked_parameter = parameters.tracked_parameter

        # initialize rest of parameters
        self._current_trial = 0
        self._current_value = self._start_values[0]
        self._current_value = self._start_values[0]
        self._current_step = self._step_sizes[0]
        self._responses: [bool] = []
        self._step_idx = 0
        self._n_per_step_count = 0
        self._n_reversals = 0
        self._track_value = []
        self._keep_going = True
        self._current_direction = 1
        self._current_corrects = 0
        self._current_incorrect = 0

        self._participant_id = ''
        self._participant_dob = None
        self._n_trials = len(self._start_values)

    def response(self, response='', target='', reaction_time=0.0):
        correct = response == target
        if not self._keep_going:
            return self._keep_going
        self._responses.append(correct)
        _is_reversal = self.is_reversal()
        if _is_reversal:
            self._n_reversals += 1
        self._track_value.append(self._current_value)
        _date = datetime.datetime.now()
        out_put = {'participant_id': self._participant_id,
                   'participant_dob': self._participant_dob,
                   'date': _date.strftime("%Y-%m-%d_%H:%M:%S"),
                   'down': self._n_correct_down,
                   'up': self._n_incorrect_up,
                   'step_index': self._step_idx,
                   'step_size': self._step_sizes[self._step_idx] if correct else self._back_step_sizes[self._step_idx],
                   'tracked_parameter': self._tracked_parameter,
                   'n_reversal': self._n_reversals,
                   'value': self._current_value,
                   'response': response,
                   'target_response': target,
                   'correct': int(correct),
                   'trial_number': self._current_trial,
                   'reaction_time': np.round(reaction_time, 3),
                   **self.extra_parameters
                   }
        self.results = self.results.append([out_put], ignore_index=True)
        to_add = 0.
        if correct:
            self._current_corrects += 1
            self._current_incorrect = 0
            if self._current_corrects >= self._n_correct_down:
                to_add = self._step_sizes[self._step_idx]
                self._current_corrects = 0.
        else:
            self._current_incorrect += 1
            self._current_corrects = 0
            if self._current_incorrect >= self._n_incorrect_up:
                self._current_incorrect = 0
                if _is_reversal:
                    # plt.plot(self._track_value)
                    self._n_per_step_count += 1
                    if self._n_per_step_count >= self._n_reversals_per_step[self._step_idx]:
                        self._step_idx += 1
                        self._n_per_step_count = 0
                    if self._step_idx < len(self._step_sizes):
                        to_add = self._back_step_sizes[self._step_idx]
                    else:
                        self._keep_going = False
                else:
                    to_add = self._back_step_sizes[self._step_idx]

        if self._keep_going:
            self._current_value += to_add
            print('c_value {:}, c_step {:}, c_back {:}, n_reversals {:},'
                  ' step_idx {:}'.format(self._current_value,
                                         self._step_sizes[self._step_idx],
                                         self._back_step_sizes[self._step_idx],
                                         self._n_reversals,
                                         self._step_idx
                                         ))
        else:
            self.results['is_reversal'] = False
            _idx = self.get_reversals_idx()
            self.results.loc[_idx, 'is_reversal'] = True
            thr, std = self.get_threshold()
            out_put = {'participant_id': self.participant_id,
                       'date': _date.strftime("%Y-%m-%d_%H:%M:%S"),
                       'down': self._n_correct_down,
                       'up': self._n_incorrect_up,
                       'reversals': np.sum(self._n_per_step_count),
                       'tracked_parameter': self._tracked_parameter,
                       'n_reversal': self._n_reversals,
                       'n_averaged': self._n_reversals_for_mean,
                       'mean': thr,
                       'sd': std,
                       'trial_number': self._current_trial,
                       **self.extra_parameters
                       }
            self.summary = self.summary.append([out_put], ignore_index=True)
            self._current_trial += 1

            if not self.finish:
                self.set_new_trial()

            print('finish at {:} reversals'.format(self._n_reversals))
        print(self._track_value)

    def is_reversal(self):
        out = False
        if len(self._responses) > 1:
            out = self._responses[-1] != self._responses[-2]
        return out

    def get_reversals_idx(self):
        _diff = np.diff(self.results.value)
        _idx_c = np.squeeze(np.argwhere(_diff != 0))
        _idx = _idx_c[np.where(np.diff(np.sign(_diff[_idx_c]), axis=0))[0]] + 1
        # append the last value
        _idx = np.append(_idx, self.results.value.size - 1)
        # plt.plot(self.results.value)
        # plt.plot(_idx, self.results.iloc[_idx, :].value, 'o')
        return _idx

    def get_threshold(self):
        _idx = self.get_reversals_idx()
        thr = self.results.iloc[_idx, :].tail(self._n_reversals_for_mean).value.mean()
        sd = self.results.iloc[_idx, :].tail(self._n_reversals_for_mean).value.std()
        return thr, sd

    def set_new_trial(self):
        self._current_value = self._start_values[self._current_trial]
        self._current_step = self._step_sizes[0]
        self._responses: [bool] = []
        self._step_idx = 0
        self._n_per_step_count = 0
        self._n_reversals = 0
        self._track_value = []
        self._keep_going = True
        self._current_direction = 1
        self._current_corrects = 0
        self._current_incorrect = 0

    def get_current_value(self):
        return self._current_value

    current_value = property(get_current_value)

    def get_trial_completed(self):
        return not self._keep_going

    trial_completed = property(get_trial_completed)

    def get_finished(self):
        return self._current_trial >= len(self._start_values)

    finish = property(get_finished)

    def get_step_sizes(self):
        return self._step_sizes

    def set_step_sizes(self, value):
        self._step_sizes = value

    step_sizes = property(get_step_sizes, set_step_sizes)

    def get_back_step_sizes(self):
        return self._back_step_sizes

    def set_back_step_sizes(self, value):
        self._back_step_sizes = value

    back_step_sizes = property(get_step_sizes, set_back_step_sizes)

    def get_down(self):
        return self._n_correct_down

    def set_down(self, value):
        self._n_correct_down = value

    down = property(get_down, set_down)

    def get_up(self):
        return self._n_incorrect_up

    def set_up(self, value):
        self._n_incorrect_up = value

    up = property(get_up, set_up)

    def get_participant_id(self):
        return self._participant_id

    def set_participant_id(self, value):
        self._participant_id = value

    participant_id = property(get_participant_id, set_participant_id)

    def get_participant_dob(self):
        return self._participant_dob

    def set_participant_dob(self, value):
        self._participant_dob = value

    participant_dob = property(get_participant_dob, set_participant_dob)

    def get_start_values(self):
        return self._start_values

    def set_start_values(self, value):
        self._start_values = value
        self._current_value = self._start_values[0]
        self._n_trials = len(self._start_values)

    start_values = property(get_start_values, set_start_values)

    def get_tracked_parameter(self):
        return self._tracked_parameter

    def set_tracked_parameter(self, value):
        self._tracked_parameter = value

    tracked_parameter = property(get_tracked_parameter, set_tracked_parameter)

    def get_reversals_per_step(self):
        return self._n_reversals_per_step

    def set_reversals_per_step(self, value):
        self._n_reversals_per_step = value

    reversals_per_step = property(get_reversals_per_step, set_reversals_per_step)

    def get_n_reversals_for_mean(self):
        return self._n_reversals_for_mean

    def set_n_reversals_for_mean(self, value):
        assert value <= self._n_reversals, 'number of reversals should be lower or equal than total number of reversals'
        self._n_reversals_for_mean = value

    reversals_for_mean = property(get_n_reversals_for_mean, set_n_reversals_for_mean)


class AudioToken(object):
    def __init__(self, data: np.array = None,
                 fs: float = 44100.,
                 rise_fall_time=0.050,
                 id=''):
        self.fs = fs
        self.id = id
        self._data = data
        self._rise_fall_time = rise_fall_time
        self._w = None
        self.set_rise_fall_window()

    def set_rms(self, rms=float):
        self._data = rms / self.rms * self._data

    def get_rms(self, thr=-40.0):
        assert thr < 0.0, 'thr should be below zero'
        _a_data = np.abs(self._data)
        _a_data = _a_data / _a_data.max()
        _a_data[_a_data < 1e-10] = 1e-10  # limit data to be higher than -200 dB
        _data_db = 20 * np.log10(_a_data)
        _idx = np.where(_data_db > thr)[0]
        _rms = np.sqrt(np.mean(self._data[_idx, :] ** 2, axis=0))
        return _rms.max()

    rms = property(get_rms, set_rms)

    def get_data(self):
        return self._data * self._w

    def set_data(self, value):
        self._data = value
        self.set_rise_fall_window()

    data = property(get_data, set_data)

    def get_rise_fall_time(self):
        return self._rise_fall_time

    rise_fall_time = property(get_rise_fall_time)

    def set_rise_fall_window(self):
        self._w = np.ones(self._data.shape)
        if self._rise_fall_time:
            _n_win = int(round(self.fs * self._rise_fall_time))
            rise_fall_samples = np.atleast_2d(np.arange(_n_win)).T
            self._w[0:_n_win, :] = np.sin(2.0 * np.pi * rise_fall_samples / (4.0 * (_n_win - 1))) ** 2.0
            self._w[-_n_win:, :] = np.cos(2.0 * np.pi * rise_fall_samples / (4.0 * (_n_win - 1))) ** 2.0

    def duration(self):
        return self._data.shape[0] / self.fs


class SNRMode(object):
    fix_masker = 'fix_masker'
    fix_target = 'fix_target'
    fix_spl = 'fix_spl'


def make_run(tokens: [AudioToken], snr=6, inter_stimuli_gap=0.2, n_tokens=3,
             itd=0.0001,
             fs=48000.,
             reference_rms=None,
             f_noise_low=100,
             f_noise_high=2000,
             duration=3.5,
             modulation_frequency=40.0,
             modulation_index=0.0,
             snr_mode=SNRMode.fix_masker
             ):
    _idx = random.choices(range(len(tokens)), k=n_tokens)
    gap = np.zeros((int(fs * inter_stimuli_gap), 2))
    _out_buffer = np.empty((0, 2))
    _tokens_ids = ''
    for _token in np.array(tokens)[_idx]:
        id, _ = os.path.splitext(_token.id)
        _tokens_ids += id
        _c_buffer = _token.data.copy()
        _c_buffer = np.atleast_2d(_c_buffer)
        if _c_buffer.shape[1] == 1:
            _c_buffer = np.hstack((_c_buffer, np.zeros(_c_buffer.shape)))
        if itd == 'inverse':
            _c_buffer[:, 1] = np.squeeze(-_c_buffer[:, 0])
        else:
            _c_buffer[:, 1] = np.squeeze(delay_signal(np.atleast_2d(_c_buffer[:, 0]).T, _token.fs, itd))
        # create temp token to apply rise_fall_time to delayed signals
        _tem_token = AudioToken(data=_c_buffer, fs=_token.fs, rise_fall_time=_token.rise_fall_time)
        _out_buffer = np.vstack((_out_buffer, _tem_token.data))
        _out_buffer = np.vstack((_out_buffer, gap))
    print(_out_buffer.shape)
    audio_token = AudioToken(data=_out_buffer, fs=fs)

    _all_tokens = np.empty((0, 2))
    for _token in np.array(tokens):
        _c_buffer = _token.data.copy()
        _c_buffer = np.atleast_2d(_c_buffer)
        if _c_buffer.shape[1] == 1:
            _c_buffer = np.hstack((_c_buffer, np.zeros(_c_buffer.shape)))
        # create temp token to apply rise_fall_time to delayed signals
        _tem_token = AudioToken(data=_c_buffer, fs=_token.fs, rise_fall_time=_token.rise_fall_time)
        _all_tokens = np.vstack((_all_tokens, _tem_token.data))
    target_data = np.atleast_2d(np.mean(_all_tokens, axis=1)).T
    fresh_noise, _ = generate_shaped_noise(fs=fs,
                                           amplitude=0.1,
                                           duration=duration,
                                           target_data=target_data,
                                           fit_signal_spectrum=False,
                                           round_next_power_2=False,
                                           reference_rms=reference_rms)

    # fresh_noise, _ = generate_modulated_noise(fs=fs,
    #                                           amplitude=0.1,
    #                                           f_noise_low=f_noise_low,
    #                                           f_noise_high=f_noise_high,
    #                                           duration=duration,
    #                                           modulation_frequency=modulation_frequency,
    #                                           modulation_index=modulation_index,
    #                                           round_to_cycle=False,
    #                                           fit_signal_spectrum=False,
    #                                           round_next_power_2=False,
    #                                           reference_rms=reference_rms)
    fresh_noise = np.tile(fresh_noise, (1, 2))
    noise_token = AudioToken(data=fresh_noise, fs=fs)

    if snr_mode == SNRMode.fix_masker or snr_mode == SNRMode.fix_spl:
        audio_token.rms = 10 ** (snr / 20) * noise_token.rms.max()
    if snr_mode == SNRMode.fix_target:
        noise_token.rms = 10 ** (-snr / 20) * audio_token.rms.max()

    print(20 * np.log10(audio_token.rms / noise_token.rms))
    _noise_samples = noise_token.data.shape[0]
    _signal_samples = audio_token.data.shape[0]
    _samp_diff = _noise_samples - _signal_samples
    _before = (_noise_samples - _signal_samples) // 2
    _after = _samp_diff - _before
    _signal_padded = np.pad(audio_token.data, ((_before, _after), (0, 0)), mode='constant', constant_values=0)
    mixed_token = AudioToken(data=noise_token.data + _signal_padded, fs=fs)
    if snr_mode == SNRMode.fix_spl:
        mixed_token.rms = reference_rms
    return mixed_token, _tokens_ids


class DigitsInNoiseRunner(QWidget):
    signal_finish = pyqtSignal(name='signal_finish')
    signal_playing_stop = pyqtSignal(name='signal_playing_stop')
    signal_playing_start = pyqtSignal(name='signal_playing_start')
    signal_runner_start = pyqtSignal(name='signal_runner_start')

    def __init__(self, parent, speaker='female_0'):
        super(DigitsInNoiseRunner, self).__init__(parent)
        self.gui = dn.DigitsInNoiseModule(self)
        self.adaptive_tracker: AdaptiveTracker = None
        self.noise_parameters: dict = {}
        self.signal_parameters: dict = {}
        self.inter_stimuli_gap: float = 0.2
        self.number_of_digits: int = 3
        self.reference_spl: float = 60
        self.exclude_tokens: [str] = ['7.wav']
        self.sound_device = None
        self.sound_device_channels: [int] = None
        self.speaker = speaker
        self._correct_answer = None
        self._data_path: str = ''
        self._date: datetime = None
        self._audio_tokens: [AudioToken] = None
        self._n_runs: int = 0
        self.__starting_time: float = 0.0
        self._reaction_time: float = 0.0
        # connect signals
        self.gui.signal_response.connect(self.get_response)
        self.signal_playing_start.connect(self.gui.start_playing)
        self.signal_playing_stop.connect(self.gui.stop_playing)
        self.gui.signal_start.connect(self.start_runner)
        self.gui.initialize('Start')

    def initialize(self, initial_message: str = 'Start', end_message: str = '',
                   adaptive_tracker_parameters: AdaptiveTrackingParameters = AdaptiveTrackingParameters()):
        self.adaptive_tracker = AdaptiveTracker(adaptive_tracker_parameters)
        self.noise_parameters = {}
        self.signal_parameters = {}
        self.inter_stimuli_gap = 0.2
        self.number_of_digits = 3
        self.reference_spl = 60
        self.exclude_tokens = ['7.wav']
        self.sound_device = 0
        self.sound_device_channels = [0, 1]
        self._correct_answer = None
        self._data_path = os.path.join(str(Path.home()), 'digits_in_noise')
        self._date = datetime.datetime.now()
        audio_files = './digits'
        data_files = [join(audio_files, f) for f in listdir(audio_files)
                      if isfile(join(audio_files, f)) and f.startswith(self.speaker) and f.endswith('.wav')]
        data_files = [_f for _e in self.exclude_tokens for _f in data_files if not _f.endswith(_e)]
        audio_tokens = []
        for i, _file_name in enumerate(data_files):
            data, samplerate = sf.read(_file_name)
            data = np.atleast_2d(data).T
            _id = re.search(r'\w+\_\d+\_(\d+)', os.path.basename(_file_name)).group(1)
            audio_tokens.append(AudioToken(data=data, fs=samplerate, id=_id))

        # make sure all tokens have the same rms
        _rms = audio_tokens[0].rms
        for _a in audio_tokens:
            _a.rms = _rms
        self._audio_tokens = audio_tokens
        self._n_runs = 0
        self.__starting_time = 0.0
        self._reaction_time = 0.0
        self.gui.initialize(initial_message, end_message)

    def play(self):
        self.signal_playing_start.emit()
        _sample, _id = make_run(tokens=self._audio_tokens,
                                snr=self.adaptive_tracker.current_value,
                                inter_stimuli_gap=self.inter_stimuli_gap,
                                n_tokens=self.number_of_digits,
                                **self.noise_parameters,
                                **self.signal_parameters)
        self._correct_answer = _id
        status = self.play_stimuli(stimuli=_sample.data, sampling_rate=_sample.fs, device=self.sound_device,
                                   channels=self.sound_device_channels)
        self.__starting_time = time.time()
        self._n_runs += 1
        print('number of runs: {:}'.format(self._n_runs))

        if status:
            print('Error during playback: ' + str(status))
        self.signal_playing_stop.emit()

    def play_stimuli(self, stimuli: np.ndarray = None, sampling_rate: float = 44100.0,
                     device=None,
                     channels=None,
                     block_size: int = 4096):
        # we detect if specific device is provided
        extra_settings = None
        mapping = None
        if self.sound_device is not None:
            device_name = sd.query_devices(self.sound_device)['name']
            sd.default.device = device
            if re.search('asio', device_name, re.IGNORECASE):
                extra_settings = sd.AsioSettings(channel_selectors=channels)
            else:
                mapping = channels
                # set sound device
        sd.play(stimuli, sampling_rate, blocking=True, blocksize=block_size, mapping=mapping,
                extra_settings=extra_settings)
        status = sd.wait()
        return status

    @pyqtSlot(str)
    def get_response(self, value: str):
        self._reaction_time = time.time() - self.__starting_time
        self.adaptive_tracker.response(response=value, target=self._correct_answer, reaction_time=self._reaction_time)
        self.gui.on_feedback(self._correct_answer == value)

        if not self.adaptive_tracker.finish:
            self.play()
        else:
            self.save_results()
            self.signal_finish.emit()
            self.gui.on_finish()

    @pyqtSlot()
    def start_runner(self):
        self.signal_runner_start.emit()

    def save_results(self):
        if not os.path.isdir(self._data_path):
            os.makedirs(self._data_path)

        _file_name = '{:}_{:}_{:}_individual.csv'.format(self.adaptive_tracker.participant_id,
                                                         self._date.strftime("%Y-%m-%d_%H_%M"),
                                                         self.reference_spl)
        self.adaptive_tracker.results.to_csv(os.path.join(self._data_path, _file_name))
        _file_name = '{:}_{:}_{:}_summary.csv'.format(self.adaptive_tracker.participant_id,
                                                      self._date.strftime("%Y-%m-%d_%H_%M"),
                                                      self.reference_spl)
        self.adaptive_tracker.summary.to_csv(os.path.join(self._data_path, _file_name))

# Digits in noise test
This is a simple interface to measure speech reception thresholds (SRT) to 
digits presented in the presence of speech-shaped noise (the shape itself is obtained from 
the audio tokens).
Data are saved in home directory under diguts_in_noise folder.


# Install
To install this code, you need to **clone** or **download** the repository.
To clone using git try:
- git clone https://gitlab.com/jundurraga/digits_in_noise.git

## Requirements
This interface has been created using python 3.
You can install requirements with pip (using the bash) running the following code
in while in the main code folder

- pip install -r requirements.txt 

or 

- pip install -r requirements.txt --user

If you use PyCharm, it will automatically suggest to install the requirements 
once you have open a new project in the main code folder and you have setup your
python environment. I assume the users know how to setup their own python 
environment.


You need to clone submodules too. First go to the folder where you downloaded
the main code and then try these options:

### Option 1 
- git submodule init
- git submodule update

### Option 2
- git clone --recurse-submodules git@gitlab.com:jundurraga/pysignal_generator.git

or

- git clone --recurse-submodules https://gitlab.com/jundurraga/pysignal_generator.git

## Option 3
- go to https://gitlab.com/jundurraga/pysignal_generator
- download the repository -
- put pysignal_renerator folder inside main code folder.


# Run the code
To run source code you can just open the project (as a folder) in PyCharm. 
Make sure to install all requirements. Importanly, you need to install Qt5, 
needed by PyQt5. This can also be done with PyCharm or pip, the same way you
would install any other package. 

## Run experiment
- Before running an experiment make sure the settings are as desired. 
This is configured in digits_in_noise_experiment.py. 
Once you have done so, run **digits_in_noise_experiment.py** to begin.